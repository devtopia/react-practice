import React from 'react'
import { render } from 'react-dom'
import ContactsAppContainer from './components/ContactsAppContainer'

render(
  <ContactsAppContainer />,
  document.getElementById('root')
)
